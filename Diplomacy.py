INIT = {}
global_state = { 'loc_cache': INIT }
targets = set([])
ARMY_STATES = { 'dead': 'dead', 'alive': 'alive' }
ACTIONS = {
    'hold': 'hold',
    'move': 'move',
    'support': 'support'
}

def get_target(target, is_army):
    if is_army:
        return global_state[target]
    if target in global_state['loc_cache']:
        cache = global_state['loc_cache']
        return global_state[cache[target]]


def kill_army(army):
    global_state[army]['is_alive'] = ARMY_STATES['dead']


def setup_actions(army_obj):
    act = army_obj['actions'][-1]
    if act['action'] == ACTIONS['hold']:
        global_state[army_obj['key']]['is_holding'] = True

    if act['action'] == ACTIONS['support']:
        target = act['target']
        other_army_obj = get_target(target, True)
        if army_obj['current_loc'] not in targets:
            # Add a valid support
            global_state[other_army_obj['key']]['supports'].append(army_obj['key'])


def execute_action(army_obj):
    act = army_obj['actions'][-1]

    if act['action'] == ACTIONS['move']:
        target = act['target']
        other_army_obj = get_target(target, False)

        # If the other army has less or equal to our support quantity,
        # kill it, then update the global state and cache accordingly.
        if len(other_army_obj['supports']) <= len(army_obj['supports']):
            other_army_loc = other_army_obj['current_loc']
            kill_army(other_army_obj['key'])
            army_key = army_obj['key']

            if len(other_army_obj['supports']) == len(army_obj['supports']):
                kill_army(army_key)
            else:
                global_state[army_key]['current_loc'] = other_army_loc
                global_state['loc_cache'][other_army_loc] = army_key
        else:
            kill_army(army_obj['key'])


def get_action(rest):
    command = rest[0].lower()
    if len(rest) == 1:
        return { 'action': command }
    # Fill target cache for support invalidation
    target = rest[1]
    if command == ACTIONS['move']:
        targets.add(target)
    return { 'action': ACTIONS[command], 'target': target }


def initialize_defaults(army, loc, rest):
    global_state[army] = {
        'key': army,
        'supports': [],
        'current_loc': loc,
        'actions': [get_action(rest)],
        'is_alive': ARMY_STATES['alive'],
        'is_holding': False
    }
    global_state['loc_cache'][loc] = army


def format_return():
    ret = ''
    for k, v in global_state.items():
        if k != 'loc_cache':
            is_alive = v['is_alive']
            if is_alive == ARMY_STATES['alive']:
                loc = v['current_loc']
                ret += k + ' ' + loc + '\n'
            else:
                ret += k + ' [dead]\n'
    return ret


def iterate_global_state(func):
    for k, v in global_state.items():
        if k != 'loc_cache':
            func(v)


def diplomacy_eval(args):
    for commands in args:
        army, loc, *rest = commands.split()

        if army not in global_state:
            initialize_defaults(army, loc, rest)

    iterate_global_state(setup_actions)
    iterate_global_state(execute_action)
    return format_return()


def diplomacy_solve(r, w):
    """
    This implmentation uses a global state dictionary,
    which contains a key-value of the army name and its
    details. As the game progresses, we check its details
    and provide the correct end state of the game.

    We want to create 2 caches, first a location cache
    to prevent global state iteration, as well as a target
    cache set to invalidate army supports when setting up the
    initial game.

    Since the game doesn't progress sequentially (D can
    support B nullifying C target B) we execute setup actions
    first like 'hold' and 'support' and note down targets
    to invalidate on the execute_action() function, which
    is primarily dealing with 'move' commands.

    We clear the global state and caches on each game.
    """
    buf = []
    for s in r:
        ns = s.strip()
        if len(s) > 1 and s != '\n':
            buf.append(ns)
        elif buf:
            ret = diplomacy_eval(buf)
            w.write(ret + '\n')
            global_state.clear()
            global_state['loc_cache'] = INIT
            targets.clear()
            buf.clear()
